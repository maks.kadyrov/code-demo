using System.Collections.Generic;
using UnityEngine;

namespace DefaultNamespace
{
    public static class VectorExtensions
    {
        public static Vector3 ToVector3(this Vector2Int vector2Int)
        {
            return new Vector3(vector2Int.x, vector2Int.y, 0);
        }

        public static List<Vector2Int> GetNeighbourPositions(this Vector2Int vector2Int)
        {
            var result = new List<Vector2Int>();
            
            result.Add(vector2Int + Vector2Int.up);
            result.Add(vector2Int + Vector2Int.right);
            result.Add(vector2Int + Vector2Int.down);
            result.Add(vector2Int + Vector2Int.left);

            return result;
        }

        public static bool IsVertical(this Vector2Int vector2Int)
        {
            return vector2Int.x == 0;
        }
        
        public static bool IsHorizontal(this Vector2Int vector2Int)
        {
            return vector2Int.y == 0;
        }
    }
}