using System.Linq;
using RLS.Signals;
using UnityEngine;
using Zenject;

namespace RLS.Gameplay
{
    public class ActorScratchesEffect : GameEffect
    {
        [Inject] private SignalBus _signalBus;
        [Inject] private Board _board;

        public bool IsVertical { get; }
        public Vector2Int Position => Owner.Position;

        private bool _isReadyToProcess = false;
        
        public ActorScratchesEffect(Actor actor, bool isVertical)
        {
            Owner = actor;
            IsVertical = isVertical;
        }

        [Inject]
        public override void Activate()
        {
            _signalBus.Subscribe<ActorDestructedSignal>(OnActorDestructed);
        }

        private void OnActorDestructed(ActorDestructedSignal signal)
        {
            if (signal.Actor != Owner) return;

            _isReadyToProcess = true;
        }

        protected override bool OnProcess()
        {
            if (!_isReadyToProcess) return false;
            
            var tilesInLine = _board.Tiles.Where(tile =>
                IsVertical ? tile.Position.x == Owner.Position.x : tile.Position.y == Owner.Position.y);

            foreach (var tile in tilesInLine)
            {
                if (tile.AttachedActor == null) continue;
                tile.AttachedActor.Destruct();
            }
            
            return true;
        }
    }
}