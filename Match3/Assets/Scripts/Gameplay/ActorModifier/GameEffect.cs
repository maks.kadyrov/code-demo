namespace RLS.Gameplay
{
    public abstract class GameEffect
    {
        protected Actor Owner;

        public bool TryProcess()
        {
            return OnProcess();
        }

        public abstract void Activate();

        protected abstract bool OnProcess();
    }
}