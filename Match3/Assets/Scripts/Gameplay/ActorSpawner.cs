using RLS.Gameplay.Factories;
using Zenject;

namespace RLS.Gameplay
{
    public class ActorSpawner
    {
        [Inject] private ActorFactory _actorFactory;

        public Actor SpawnActorAt(ActorType type, Tile tile)
        {
            var actor = _actorFactory.Create(type, tile.Position);
            tile.AttachActor(actor);
            return actor;
        }
    }
}