using UnityEngine;

namespace RLS.Gameplay.Behaviours
{
    public class ActorBehaviour : MonoBehaviour
    {
        public Actor Actor { get; private set; }

        [SerializeField] private SpriteRenderer _spriteRenderer; 

        public void Init(Actor actor)
        {
            Actor = actor;
        }
    }
}