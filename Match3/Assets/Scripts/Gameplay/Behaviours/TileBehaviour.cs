using RLS.Gameplay.ActorParameters;
using RLS.InputSystem;
using UnityEngine;
using Zenject;

namespace RLS.Gameplay.Behaviours
{
    public class TileBehaviour : MonoBehaviour, IDropReceiver
    {
        [Inject] private ActionProcessor _actionProcessor;

        private Tile _tile;

        public void Init(Tile tile)
        {
            _tile = tile;
        }

        public bool TryReceive(Actor actor)
        {
            if (_tile.AttachedActor is not IDraggable) return false;
                
            if (IsNeighbourActor(actor))
            {
                _actionProcessor.Process(new SwapForwardAction(actor, _tile.AttachedActor));
                return true;
            }

            return false;
        }

        private bool IsNeighbourActor(Actor actor)
        {
            return _tile.IsNeighbourActor(actor);

        }
    }
}