using Zenject;

namespace RLS.Gameplay
{
    public class ActorActionProcessor : GameProcessorBase
    {
        private Board _board;

        public ActorActionProcessor(Board board, SignalBus signalBus) : base(signalBus)
        {
            _board = board;
        }

        public override void Process()
        {
            foreach (var tile in _board.Tiles)
            {
                tile.AttachedActor?.Process();
            }
        }
    }
}