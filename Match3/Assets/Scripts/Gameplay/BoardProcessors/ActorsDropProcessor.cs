using RLS.Gameplay.ActorParameters;
using RLS.Signals;
using UnityEngine;
using Zenject;

namespace RLS.Gameplay
{
    public class ActorsDropProcessor : GameProcessorBase
    {
        private Board _board;
        
        public ActorsDropProcessor(Board board, SignalBus signalBus) : base(signalBus)
        {
            _board = board;
        }

        public override void Process()
        {
            for (int y = _board.MinPoint.y; y <= _board.MaxPoint.y; y++)
            {
                for (int x = _board.MinPoint.x; x <= _board.MaxPoint.x; x++)
                {
                    if (_board.TryGetTileAt(new Vector2Int(x, y), out var tile))
                    {
                        if (tile.AttachedActor != null) continue;

                        DropInTile(tile);
                    }
                }
            }
        }
        
        private void DropInTile(Tile tile)
        {
            var x = tile.Position.x;
            var startY = tile.Position.y;

            for (int y = startY; y <= _board.MaxPoint.y; y++)
            {
                if (_board.TryGetTileAt(new Vector2Int(x, y), out var dropTile))
                {
                    var actor = dropTile.AttachedActor;

                    if (actor is not IDraggable) continue;

                    dropTile.DetachActor();
                    tile.AttachActor(actor);
                    
                    SignalBus.Fire(new ActorMovedSignal(actor, tile));
                    return;
                }
            }
        }
    }
}