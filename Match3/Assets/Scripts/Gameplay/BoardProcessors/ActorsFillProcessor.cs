using Zenject;

namespace RLS.Gameplay
{
    public class ActorsFillProcessor : GameProcessorBase
    {
        private Board _board;
        private ActorSpawner _actorSpawner;
        
        public ActorsFillProcessor(Board board, ActorSpawner actorSpawner, SignalBus signalBus) : base(signalBus)
        {
            _board = board;
            _actorSpawner = actorSpawner;
        }

        public override void Process()
        {
            foreach (var tile in _board.Tiles)
            {
                if (tile.AttachedActor == null)
                {
                    _actorSpawner.SpawnActorAt(ActorType.Random, tile);
                }
            }
        }
    }
}