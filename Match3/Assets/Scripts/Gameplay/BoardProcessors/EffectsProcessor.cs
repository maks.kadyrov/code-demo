using System.Collections.Generic;
using RLS.Signals;
using Zenject;

namespace RLS.Gameplay
{
    public class EffectsProcessor : GameProcessorBase
    {
        [Inject] private SignalBus _signalBus;
        
        private DiContainer _container;
        private List<GameEffect> _effects = new List<GameEffect>();
        
        public EffectsProcessor(SignalBus signalBus) : base(signalBus){}

        [Inject]
        private void Construct(DiContainer container)
        {
            _container = container;
        }
        
        public override void Process()
        {
            for (int i = 0; i < _effects.Count; i++)
            {
                if (_effects[i].TryProcess())
                {
                    _signalBus.Fire(new EffectProcessedSignal(_effects[i]));
                    _effects.RemoveAt(i);
                    i--;
                }
            }
        }

        public void AddEffect(GameEffect effect)
        {
            _container.Inject(effect);
            _effects.Add(effect);
        }
    }
}