using Zenject;

namespace RLS.Gameplay
{
    public abstract class GameProcessorBase
    {
        protected readonly SignalBus SignalBus;
        
        public GameProcessorBase(SignalBus signalBus)
        {
            SignalBus = signalBus;
        }

        public abstract void Process();
    }
}