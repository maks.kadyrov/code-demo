using System.Linq;

namespace RLS.Gameplay.BonusMatches
{
    public class Match4Bonus : MatchBonus
    {
        protected override int TargetCount => 4;

        private ActorSpawner _actorSpawner;

        public Match4Bonus(ActorSpawner actorSpawner, EffectsProcessor effectsProcessor) : base(effectsProcessor)
        {
            _actorSpawner = actorSpawner;
        }
        
        public override bool CanApply(MatchChain chain)
        {
            return chain.ActorsInChain.Count == TargetCount;
        }

        public override void Apply(MatchChain chain)
        {
            var actors = chain.ActorsInChain;
            
            if (chain.ActorsInChain.First().TryGetTile(out var tile))
            {
                var actor = _actorSpawner.SpawnActorAt(chain.ActorsType, tile);
                var isVertical = actors.First().Position.x != actors.Last().Position.x;
                EffectsProcessor.AddEffect(new ActorScratchesEffect(actor, isVertical));
            }
        }
    }
}