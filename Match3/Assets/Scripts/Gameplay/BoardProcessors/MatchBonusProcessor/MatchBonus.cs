namespace RLS.Gameplay.BonusMatches
{
    public abstract class MatchBonus
    {
        protected EffectsProcessor EffectsProcessor { get; private set; }
        
        public MatchBonus(EffectsProcessor effectsProcessor)
        {
            EffectsProcessor = effectsProcessor;
        }
        
        protected abstract int TargetCount { get; }
        public abstract bool CanApply(MatchChain chain);
        public abstract void Apply(MatchChain chain);
    }
}