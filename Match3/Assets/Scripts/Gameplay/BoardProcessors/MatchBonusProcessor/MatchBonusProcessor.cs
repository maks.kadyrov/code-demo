using System;
using System.Collections.Generic;
using RLS.Gameplay.BonusMatches;
using Zenject;

namespace RLS.Gameplay
{
    public class MatchBonusProcessor : GameProcessorBase
    {
        [Inject] private ActorSpawner _actorSpawner;
        [Inject] private EffectsProcessor _effectsProcessor;
        
        private MatchProcessor _matchProcessor;
        private List<MatchBonus> _bonuses;

        private Action _onProcess;
        
        public MatchBonusProcessor(MatchProcessor matchProcessor, SignalBus signalBus) : base(signalBus)
        {
            _matchProcessor = matchProcessor;
        }

        [Inject]
        private void Construct()
        {
            _bonuses = new List<MatchBonus>()
            {
                new Match4Bonus(_actorSpawner, _effectsProcessor),
            };
        }

        public void Prepare()
        {
            var chains = _matchProcessor.GetAllMatchChains();

            foreach (var chain in chains)
            {
                foreach (var bonus in _bonuses)
                {
                    if (bonus.CanApply(chain))
                    {
                        _onProcess += () => bonus.Apply(chain);
                    }
                }
            }
        }
        
        public override void Process()
        {
            _onProcess?.Invoke();
            _onProcess = null;
        }
    }
}