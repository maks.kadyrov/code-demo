using System.Collections.Generic;
using System.Linq;
using Unity.VisualScripting;

namespace RLS.Gameplay
{
    public class MatchChain
    {
        public ActorType ActorsType { get; private set; }
        
        public HashSet<Actor> ActorsInChain => new HashSet<Actor>(_actorsInChain);
        private HashSet<Actor> _actorsInChain = new HashSet<Actor>();

        public void AddRange(HashSet<Actor> actors)
        {
            ActorsType = actors.First().Type;
            _actorsInChain.AddRange(actors);
        }

        public void Match()
        {
            foreach (var actor in _actorsInChain)
            {
                actor.Destruct();
            }
        }
    }
}