using System.Collections.Generic;
using System.Linq;
using RLS.Gameplay.ActorParameters;
using RLS.Signals;
using Unity.VisualScripting;
using UnityEngine;
using Zenject;

namespace RLS.Gameplay
{
    public class MatchProcessor : GameProcessorBase
    {
        private Board _board;

        private HashSet<Actor> _lockedActors = new HashSet<Actor>();

        public MatchProcessor(Board board, SignalBus signalBus) : base(signalBus)
        {
            _board = board;
        }
        
        public override void Process()
        {
            var matchedActors = new HashSet<Actor>();
            var chains = GetAllMatchChains();

            foreach (var chain in chains)
            {
                matchedActors.AddRange(chain.ActorsInChain);
            }
            
            if (chains.Count > 0)
            {
                ApplyMatch(chains);
            }
        }

        private void ApplyMatch(List<MatchChain> chains)
        {
            foreach (var chain in chains)
            {
                chain.Match();
            }
        }

        public bool IsAnyMergeAvailable()
        {
            return GetAllMatchChains().Any();
        }

        public List<MatchChain> GetAllMatchChains()
        {
            var result = new List<MatchChain>();
            
            foreach (var tile in _board.Tiles)
            {
                if (_lockedActors.Contains(tile.AttachedActor)) continue;
                
                if (TryGetMatchChainFor(tile.AttachedActor, out var chain))
                {
                    _lockedActors.AddRange(chain.ActorsInChain);
                    result.Add(chain);
                }
            }
            
            _lockedActors.Clear();
            return result;
        }

        private bool TryGetMatchChainFor(Actor actor, out MatchChain chain)
        {
            chain = new MatchChain();
            var matchedActors = CalculateMatch(actor);
                
            if (!matchedActors.Any()) return false;
            
            chain.AddRange(matchedActors);
            
            foreach (var matchedActor in matchedActors)
            {
                chain.AddRange(CalculateMatch(matchedActor));
            }
            
            return true;
        }

        private bool IsEnoughToMatch(HashSet<Actor> actors)
        {
            return actors.Count >= 3;
        }

        private HashSet<Actor> CalculateMatch(Actor actor)
        {
            var result = new HashSet<Actor>();
            result.AddRange(GetMatchInLine(actor, true));
            result.AddRange(GetMatchInLine(actor, false));
            return result;
        }

        private HashSet<Actor> GetMatchInLine(Actor actor, bool isVertical)
        {
            var result = new HashSet<Actor>();

            var firstDirection = isVertical ? Vector2Int.up : Vector2Int.right;
            var secondDirection = isVertical ? Vector2Int.down : Vector2Int.left;

            result.Add(actor);
            result.AddRange(CheckMatchInDirection(actor, firstDirection));
            result.AddRange(CheckMatchInDirection(actor, secondDirection));

            if (!IsEnoughToMatch(result))
            {
                result.Clear();
            }

            return result;
        }
        
        private HashSet<Actor> CheckMatchInDirection(Actor actor, Vector2Int direction)
        {
            var result = new HashSet<Actor>();

            Tile currentTile;
            var currentPosition = actor.Position;
            var currentActor = actor;
            
            while (currentActor.Type == actor.Type)
            {
                currentPosition += direction;
                
                if (_board.TryGetTileAt(currentPosition, out currentTile))
                {
                    if (!currentTile.IsPlayable) continue;

                    currentActor = currentTile.AttachedActor;
                    
                    if (currentActor is not IMatchable) continue;

                    if (currentActor.Type == actor.Type)
                    {
                        result.Add(currentActor);
                    }
                }
                else
                {
                    break;
                }
            }

            return result;
        }
    }
}