using DefaultNamespace;
using RLS.Signals;
using UnityEngine;
using Zenject;

namespace RLS.Gameplay
{
    public class CameraBehaviour : MonoBehaviour
    {
        [Inject] private SignalBus _signalBus;

        [SerializeField] private Camera _camera;

        private void Awake()
        {
            _signalBus.Subscribe<BoardSetupSignal>(OnBoardSetup);
        }

        private void OnBoardSetup(BoardSetupSignal signal)
        {
            var board = signal.Board;
            var targetPosition = (board.MaxPoint + board.MinPoint).ToVector3() / 2;
            targetPosition.z = -10;
            transform.position = targetPosition;

            _camera.orthographicSize = board.MaxPoint.x - board.MinPoint.x + 2;
        }
    }
}