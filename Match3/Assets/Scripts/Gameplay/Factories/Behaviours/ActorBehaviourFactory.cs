using System;
using RLS.Gameplay.Behaviours;
using RLS.Libraries;
using Zenject;

namespace RLS.Gameplay.Factories
{
    public class ActorBehaviourFactory : PlaceholderFactory<Actor, ActorBehaviour>{}
    
    public class CustomActorBehaviourFactory : IFactory<Actor, ActorBehaviour>
    {
        private DiContainer _container;
        private ActorsLibrary _actorsLibrary;
        
        public CustomActorBehaviourFactory(DiContainer container, ActorsLibrary actorsLibrary)
        {
            _container = container;
            _actorsLibrary = actorsLibrary;
        }
        
        public ActorBehaviour Create(Actor actor)
        {
            if (actor == null)
            {
                throw new ArgumentNullException();
            }

            if (_actorsLibrary.TryGetData(actor.Type, out var actorLibraryData))
            {
                var actorPrefab = actorLibraryData.Prefab;
                var actorBehaviour = _container.InstantiatePrefabForComponent<ActorBehaviour>(actorPrefab);
                actorBehaviour.Init(actor);

                return actorBehaviour;
            }
            
            return null;
        }
    }
}