using System;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using Random = UnityEngine.Random;

namespace RLS.Gameplay.Factories
{
    public class ActorFactory : PlaceholderFactory<ActorType, Vector2Int, Actor>{}
    
    public class CustomActorFactory : IFactory<ActorType, Vector2Int, Actor>
    {
        private DiContainer _container;
        private SignalBus _signalBus;

        private List<ActorType> _randomPool = new()
        {
            ActorType.BlueTriangle,
            ActorType.RedTriangle,
            ActorType.GreenTriangle,
            ActorType.PurpleTriangle
        };
        
        public CustomActorFactory(DiContainer container, SignalBus signalBus)
        {
            _container = container;
            _signalBus = signalBus;
        }
        
        public Actor Create(ActorType type, Vector2Int position)
        {
            type = type.Equals(ActorType.Random) ? PickRandomType() : type;
            var actor = _container.Instantiate(GetModel(type), new object[] {type, position}) as Actor;
            actor.Install(_signalBus);
            return actor;
        }

        private ActorType PickRandomType()
        {
            var rand = Random.Range(0, _randomPool.Count);
            return _randomPool[rand];
        }

        private Type GetModel(ActorType type)
        {
            switch (type)
            {
                case ActorType.Box:
                    return typeof(BoxActor);
                default:
                    return typeof(CrystalActor);
            }
        }
    }
    
}