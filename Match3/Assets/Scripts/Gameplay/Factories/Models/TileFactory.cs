using UnityEngine;
using Zenject;

namespace RLS.Gameplay.Factories
{
    public class TileFactory : PlaceholderFactory<Vector2Int, Tile>{}

    public class CustomTileFactory : IFactory<Vector2Int, Tile>
    {
        private SignalBus _signalBus;
        private DiContainer _container;

        public CustomTileFactory(DiContainer container, SignalBus signalBus)
        {
            _container = container;
            _signalBus = signalBus;
        }

        public Tile Create(Vector2Int position)
        {
            var tile = _container.Instantiate<Tile>(new object[] {position});
            tile.Install(_signalBus);
            return tile;
        }
    }
}