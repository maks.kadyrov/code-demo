using RLS.VisualProcesses;
using Zenject;

namespace RLS.Gameplay
{
    public class ActionProcessor
    {
        private DiContainer _container;

        [Inject] private VisualProcessor _visualProcessor;
        [Inject] private MatchProcessor _matchProcessor;
        [Inject] private MatchBonusProcessor _matchBonusProcessor;
        [Inject] private ActorsDropProcessor _actorDropProcessor;
        [Inject] private ActorsFillProcessor _actorsFillProcessor;
        [Inject] private ActorActionProcessor _actorActionProcessor;
        [Inject] private EffectsProcessor _effectsProcessor;

        [Inject]
        private void Construct(DiContainer container)
        {
            _container = container;
        }

        public void Process(GameAction action)
        {
            if (action != null)
            {
                _container.Inject(action);
                action.Process();
            }

            RunProcesses();
        }

        private void RunProcesses()
        {
            _matchBonusProcessor.Prepare();
            _matchProcessor.Process();
            _actorActionProcessor.Process();
            _effectsProcessor.Process();
            _visualProcessor.StartNewChain();
            
            _matchBonusProcessor.Process();
            _visualProcessor.StartNewChain();
            
            _actorDropProcessor.Process();
            _actorsFillProcessor.Process();
            _visualProcessor.StartNewChain();
            
            if (_matchProcessor.IsAnyMergeAvailable())
            {
                RunProcesses();
            }
        }
    }
}