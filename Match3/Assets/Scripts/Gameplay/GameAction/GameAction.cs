using RLS.Signals;
using Zenject;

namespace RLS.Gameplay
{
    public abstract class GameAction
    {
        [Inject] protected SignalBus SignalBus;
        
        public void Process()
        {
            OnProcess();
            SignalBus.Fire(new ActionProcessedSignal(this));
        }
        public abstract void OnProcess();
    }
}