using RLS.Signals;
using Zenject;

namespace RLS.Gameplay
{
    public abstract class SwapAction : GameAction
    {
        [Inject] private Board _board;

        public readonly Actor FirstActor;
        public readonly Actor SecondActor;

        public SwapAction(Actor firstActor, Actor secondActor)
        {
            FirstActor = firstActor;
            SecondActor = secondActor;
        }
        
        public override void OnProcess()
        {
            if (!_board.TryGetTileAt(FirstActor.Position, out var firstTile)) return;
            if (!_board.TryGetTileAt(SecondActor.Position, out var secondTile)) return;
            
            firstTile.DetachActor();
            secondTile.DetachActor();
            
            secondTile.AttachActor(FirstActor);
            firstTile.AttachActor(SecondActor);

            SignalBus.Fire(new ActorMovedSignal(FirstActor, secondTile));
            SignalBus.Fire(new ActorMovedSignal(SecondActor, firstTile));
        }
    }
}