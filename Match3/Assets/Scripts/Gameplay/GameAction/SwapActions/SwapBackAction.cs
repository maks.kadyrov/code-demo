namespace RLS.Gameplay
{
    public class SwapBackAction : SwapAction
    {
        public SwapBackAction(Actor firstActor, Actor secondActor) : base(firstActor, secondActor){}
    }
}