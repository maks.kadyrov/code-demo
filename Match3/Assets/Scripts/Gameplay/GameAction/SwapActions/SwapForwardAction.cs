using RLS.VisualProcesses;
using Zenject;

namespace RLS.Gameplay
{
    public class SwapForwardAction : SwapAction
    {
        [Inject] private MatchProcessor _matchProcessor;
        [Inject] private ActionProcessor _actionProcessor;
        [Inject] private VisualProcessor _visualProcessor;

        public SwapForwardAction(Actor firstActor, Actor secondActor) : base(firstActor, secondActor){}

        public override void OnProcess()
        {
            base.OnProcess();

            if (!_matchProcessor.IsAnyMergeAvailable())
            {
                _visualProcessor.StartNewChain();
                _actionProcessor.Process(new SwapBackAction(FirstActor, SecondActor));
            }
        }
    }
}