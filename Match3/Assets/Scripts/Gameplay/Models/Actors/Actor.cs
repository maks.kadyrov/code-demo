using System;
using RLS.Signals;
using UnityEngine;
using Zenject;

namespace RLS.Gameplay
{
    public class Actor
    {
        [Inject] private Board _board;
        
        public ActorType Type { get; private set; }
        public Vector2Int Position { get; private set; }
        protected SignalBus SignalBus { get; private set; }
        protected Action OnProcess;

        public void Install(SignalBus signalBus)
        {
            SignalBus = signalBus;
            SignalBus.Fire(new ActorSpawnSignal(this));
            OnInstall();
        }

        protected virtual void OnInstall(){}

        public Actor(ActorType type, Vector2Int position)
        {
            Type = type;
            Position = position;
        }

        public void SetPosition(Vector2Int position)
        {
            Position = position;
        }

        public void Destruct()
        {
            if (_board.TryGetTileAt(Position, out var tile))
            {
                tile.DetachActor();
            }
            
            SignalBus.Fire(new ActorDestructedSignal(this));
        }

        public void Process()
        {
            OnProcess?.Invoke();
            OnProcess = null;
        }

        public bool TryGetTile(out Tile tile)
        {
            return _board.TryGetTileAt(Position, out tile);
        }
    }

    public enum ActorType
    {
        Undefined = 0,
        Random = 1,
        RedTriangle,
        GreenTriangle,
        BlueTriangle,
        PurpleTriangle,
        Box,
    }
}