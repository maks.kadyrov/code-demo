using System;
using RLS.Signals;
using UnityEngine;
using Zenject;

namespace RLS.Gameplay
{
    public class BoxActor : Actor
    {
        [Inject] private Board _board;

        public BoxActor(ActorType type, Vector2Int position) : base(type, position){}

        private Action _onActorDestructed;

        protected override void OnInstall()
        {
            SignalBus.Subscribe<ActorDestructedSignal>(OnActorDestructed);
        }

        private void OnActorDestructed(ActorDestructedSignal signal)
        {
            if (signal.Actor is BoxActor) return;
            
            if (_board.TryGetTileAt(Position, out var tile))
            {
                if (!tile.IsNeighbourActor(signal.Actor)) return;

                OnProcess = Destruct;
            }
        }
    }
}