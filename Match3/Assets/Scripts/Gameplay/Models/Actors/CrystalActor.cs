using RLS.Gameplay.ActorParameters;
using UnityEngine;

namespace RLS.Gameplay
{
    public class CrystalActor : Actor, IDraggable, IMatchable
    {
        public CrystalActor(ActorType type, Vector2Int position) : base(type, position){}
    }
}