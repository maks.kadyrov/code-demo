using System.Collections.Generic;
using System.Linq;
using RLS.Editor;
using RLS.Gameplay.Factories;
using RLS.Signals;
using UnityEngine;
using Zenject;

namespace RLS.Gameplay
{
    public class Board
    {
        [Inject] private TileFactory _tileFactory;
        [Inject] private SignalBus _signalBus;
        [Inject] private ActorSpawner _actorSpawner;
        [Inject] private ActionProcessor _actionProcessor;

        public Vector2Int MinPoint { get; private set; } = Vector2Int.zero;
        public Vector2Int MaxPoint { get; private set; } = Vector2Int.zero;

        public List<Tile> Tiles => _tiles.Values.ToList();
        private Dictionary<Vector2Int, Tile> _tiles = new Dictionary<Vector2Int, Tile>();

        public void Setup(LevelSetup levelSetup)
        {
            foreach (var setupActor in levelSetup.SetupActors)
            {
                UpdateCornerPoints(setupActor.Position);

                var tile = _tileFactory.Create(setupActor.Position);
                tile.SetPlayable();
                _tiles.Add(setupActor.Position, tile);

                _actorSpawner.SpawnActorAt(setupActor.ActorType, tile);
            }

            _signalBus.Fire(new BoardSetupSignal(this));
            _signalBus.Fire<BoardUpdatedSignal>();
            
            _actionProcessor.Process(null);
        }
        
        private void UpdateCornerPoints(Vector2Int position)
        {
            var maxX = position.x > MaxPoint.x ? position.x : MaxPoint.x;
            var maxY = position.y > MaxPoint.y ? position.y : MaxPoint.y;
            var minX = position.x < MinPoint.x ? position.x : MinPoint.x;
            var minY = position.y < MinPoint.y ? position.y : MinPoint.y;

            MaxPoint = new Vector2Int(maxX, maxY);
            MinPoint = new Vector2Int(minX, minY);
        }
        
        public bool TryGetTileAt(Vector2Int position, out Tile tile)
        {
            tile = null;
            
            if (_tiles.ContainsKey(position))
            {
                tile = _tiles[position];
                return true;
            }

            return false;
        }
    }
}