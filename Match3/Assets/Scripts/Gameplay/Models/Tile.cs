using RLS.Signals;
using UnityEngine;
using Zenject;

namespace RLS.Gameplay
{
    public class Tile
    {
        public Vector2Int Position { get; private set; }
        public bool IsPlayable { get; private set; }
        public Actor AttachedActor { get; private set; }
        
        private SignalBus _signalBus;

        public Tile(Vector2Int position)
        {
            Position = position;
        }
        
        public void Install(SignalBus signalBus)
        {
            _signalBus = signalBus;
            _signalBus.Fire(new TileCreatedSignal(this));
        }
        
        public void SetPlayable()
        {
            IsPlayable = true;
        }

        public void AttachActor(Actor actor)
        {
            AttachedActor = actor;
            AttachedActor.SetPosition(Position);
        }

        public void DetachActor()
        {
            AttachedActor = null;
        }
        
        public bool IsNeighbourActor(Actor actor)
        {
            return Mathf.Abs(Position.x - actor.Position.x) == 1
                   && Position.y == actor.Position.y
                   || Mathf.Abs(Position.y - actor.Position.y) == 1
                   && Position.x == actor.Position.x;

        }
    }
}