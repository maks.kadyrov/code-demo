using DG.Tweening;

namespace RLS.VisualProcesses
{
    public class VisualProcess
    {
        private Tween _sequence;
        
        public VisualProcess(Tween sequence)
        {
            sequence.Pause();
            _sequence = sequence;
        }

        public Tween Process()
        {
            return _sequence.Play();
        }
    }
}