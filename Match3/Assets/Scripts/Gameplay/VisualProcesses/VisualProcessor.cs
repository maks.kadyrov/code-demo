using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using UnityEngine;

namespace RLS.VisualProcesses
{
    public class VisualProcessor : MonoBehaviour
    {
        private List<List<VisualProcess>> _behaviourProcesses = new List<List<VisualProcess>>();

        private void Awake()
        {
            StartCoroutine(Processing());
        }
        
        public void Join(VisualProcess process)
        {
            if (_behaviourProcesses.Count == 0)
            {
                StartNewChain();
            }
            
            _behaviourProcesses.Last().Add(process);
        }

        public void StartNewChain()
        {
            _behaviourProcesses.Add(new List<VisualProcess>());
        }

        public IEnumerator Processing()
        {
            while (gameObject != null)
            {
                yield return null;
                
                if (_behaviourProcesses.Count == 0) continue;
                
                var sequence = DOTween.Sequence();

                foreach (var behaviourProcess in _behaviourProcesses.First())
                {
                    sequence.Join(behaviourProcess.Process());
                }
                
                _behaviourProcesses.RemoveAt(0);
                
                yield return sequence.Play().WaitForCompletion();
            }
        }
    }
}