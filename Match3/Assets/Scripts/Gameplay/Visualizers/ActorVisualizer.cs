using System.Collections.Generic;
using DefaultNamespace;
using DG.Tweening;
using RLS.Gameplay.Factories;
using RLS.Signals;
using RLS.VisualProcesses;
using UnityEngine;
using Zenject;

namespace RLS.Gameplay.Behaviours
{
    public class ActorVisualizer : VisualizerBase
    {
        [Inject] private Board _board;
        [Inject] private SignalBus _signalBus;
        [Inject] private ActorBehaviourFactory _actorBehaviourFactory;
        [Inject] private VisualProcessor _visualProcessor;

        private Dictionary<Actor, ActorBehaviour> _actors = new Dictionary<Actor, ActorBehaviour>();
        
        public override void Initialize()
        {
            _signalBus.Subscribe<ActionProcessedSignal>(OnActionProcessed);
            _signalBus.Subscribe<ActorDestructedSignal>(OnActorDestructed);
            _signalBus.Subscribe<ActorMovedSignal>(OnActorMoved);
            _signalBus.Subscribe<ActorSpawnSignal>(OnActorSpawned);
        }

        private void OnActorSpawned(ActorSpawnSignal signal)
        {
            SpawnActor(signal.Actor);
        }

        private void OnActorMoved(ActorMovedSignal signal)
        {
            FixPosition(_actors[signal.Actor]);
        }

        private void OnActorDestructed(ActorDestructedSignal signal)
        {
            var actor = signal.Actor;

            var actorBehaviour = _actors[actor];

            var sequence = DOTween.Sequence();
            sequence.Append(actorBehaviour.transform.DOScale(0, .5f).SetEase(Ease.InQuint));
            sequence.AppendCallback(() => Object.Destroy(actorBehaviour.gameObject));

            _visualProcessor.Join(new VisualProcess(sequence));
            _actors.Remove(actor);
        }
        
        private void SpawnActor(Actor actor)
        {
            var actorBehaviour = _actorBehaviourFactory.Create(actor);
            actorBehaviour.transform.localScale = Vector3.zero;

            var spawnPosition = new Vector3(actor.Position.x, _board.MaxPoint.y + 1, 0);
            actorBehaviour.transform.position = spawnPosition;
            
            _actors.Add(actor, actorBehaviour);
            
            if (actorBehaviour)
            {
                FixScale(actorBehaviour);
                FixPosition(actorBehaviour);
            }
            else
            {
                Debug.LogError($"Can't spawn actor {actor.Type}");
            }
        }


        private void OnActionProcessed(ActionProcessedSignal signal)
        {
            if (signal.Is<SwapAction>(out var swap))
            {
                _visualProcessor.StartNewChain();
                FixPosition(_actors[swap.FirstActor]);
                FixPosition(_actors[swap.SecondActor]);
            }
        }

        private void FixScale(ActorBehaviour actorBehaviour)
        {
            var duration = .1f;
            _visualProcessor.Join(new VisualProcess(actorBehaviour.transform.DOScale(Vector3.one, duration)));
        }

        private void FixPosition(ActorBehaviour actorBehaviour)
        {
            var sequence = DOTween.Sequence();
            var targetPosition = actorBehaviour.Actor.Position.ToVector3();
            sequence.Append(actorBehaviour.transform.DOMove(targetPosition, .2f).SetEase(Ease.OutQuint));
            _visualProcessor.Join(new VisualProcess(sequence));
        }

        private void OnDestroy()
        {
            _signalBus.TryUnsubscribe<ActionProcessedSignal>(OnActionProcessed);
        }
    }
}