using RLS.Editor;
using UnityEngine;
using Zenject;

namespace RLS.Gameplay.Behaviours
{
    public class BoardVisualizer : VisualizerBase
    {
        [Inject] private Board _board;

        [SerializeField] private LevelSetup _levelSetup; //TODO: Move out from here

        public override void Initialize()
        {
            _board.Setup(_levelSetup);
        }
    }
}

