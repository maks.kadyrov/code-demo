using System.Collections.Generic;
using RLS.Gameplay;
using UnityEngine;

namespace Gameplay.Visualizers
{
    public class EffectsVisualizer : VisualizerBase
    {
        [SerializeField] private List<EffectsVisualizerModule> _modules;
        
        public override void Initialize()
        {
            foreach (var module in _modules)
            {
                module.Initialize();
            }
        }
    }
}