using UnityEngine;

namespace Gameplay.Visualizers
{
    public abstract class EffectsVisualizerModule : MonoBehaviour
    {
        public abstract void Initialize();
    }
}