using DefaultNamespace;
using DG.Tweening;
using RLS.Gameplay;
using RLS.Signals;
using RLS.VisualProcesses;
using UnityEngine;
using Zenject;

namespace Gameplay.Visualizers
{
    public class ScratchesVisualizerModule : EffectsVisualizerModule
    {
        [Inject] private Board _board;
        [Inject] private SignalBus _signalBus;
        [Inject] private VisualProcessor _visualProcessor;

        private const float Duration = .4f;

        [SerializeField] private GameObject _scratchesEffect;

        public override void Initialize()
        {
            _signalBus.Subscribe<EffectProcessedSignal>(OnEffectProcessed);
        }

        private void OnEffectProcessed(EffectProcessedSignal signal)
        {
            var effect = signal.Effect as ActorScratchesEffect;
            
            if (effect == null) return;

            var startPosition = effect.Position;
            var finishPosition = startPosition;

            if (effect.IsVertical)
            {
                startPosition.y = _board.MaxPoint.y + 1;
                finishPosition.y = _board.MinPoint.y - 1;
            }
            else
            {
                startPosition.x = _board.MinPoint.x - 1;
                finishPosition.x = _board.MaxPoint.x + 1;
            }

            var sequence = DOTween.Sequence();

            var item = Instantiate(_scratchesEffect, startPosition.ToVector3(), Quaternion.identity);
            item.transform.localScale = Vector3.zero;

            sequence.AppendCallback(() =>
            {
                item.transform.localScale = Vector3.one;
                item.transform.Rotate(Vector3.forward * 90);
            });
            sequence.Append(item.transform.DOMove(finishPosition.ToVector3(), Duration));
            sequence.AppendCallback(() => Destroy(item));
            
            _visualProcessor.Join(new VisualProcess(sequence));
        }
    }
}