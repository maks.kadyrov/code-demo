using System;
using RLS.Signals;
using UnityEngine;
using Zenject;

namespace RLS.Gameplay.Behaviours
{
    public class TileVisualizer : VisualizerBase
    {
        [Inject] private SignalBus _signalBus;

        [SerializeField] private TileBehaviour _tileBehaviourPrefab;

        private Func<TileBehaviour, TileBehaviour> _spawnTile;

        [Inject]
        private void Construct(DiContainer container)
        {
            _spawnTile = container.InstantiatePrefabForComponent<TileBehaviour>;
        }
        
        public override void Initialize()
        {
            _signalBus.Subscribe<TileCreatedSignal>(OnTileCreated);
        }

        private void OnTileCreated(TileCreatedSignal signal)
        {
            var tile = _spawnTile(_tileBehaviourPrefab);
            tile.Init(signal.Tile);
            var tilePosition = signal.Tile.Position;
            tile.transform.position = new Vector3(tilePosition.x, tilePosition.y, 0);
        }


    }
}