using UnityEngine;

namespace RLS.Gameplay
{
    public abstract class VisualizerBase : MonoBehaviour
    {
        public abstract void Initialize();
    }
}

