using System.Collections.Generic;
using UnityEngine;

namespace RLS.Gameplay
{
    public class VisualizersManager : MonoBehaviour
    {
        [SerializeField] private List<VisualizerBase> _orederedVisualizers;

        private void Awake()
        {
            foreach (var visualizer in _orederedVisualizers)
            {
                visualizer.Initialize();
            }
        }
    }
}

