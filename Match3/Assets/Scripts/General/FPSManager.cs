using UnityEngine;

namespace RLS.General
{
    public class FPSManager : MonoBehaviour
    {
        private void Awake()
        {
            Application.targetFrameRate = 60;
        }
    }
}