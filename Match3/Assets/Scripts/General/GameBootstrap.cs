using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace RLS.General
{
    public class GameBootstrap : MonoBehaviour
    {
        private void Awake()
        {
            StartCoroutine(SceneLoading());
        }

        private IEnumerator SceneLoading()
        {
            yield return new WaitForSeconds(1f);
            SceneManager.LoadScene("gameplay_scene");
        }
    }
}