namespace RLS.General
{
    public class SortingLayers
    {
        public const string Background = "Background";
        public const string Tile = "Tile";
        public const string Actor = "Actor";
        public const string MovingActor = "MovingActor";
    }
}