namespace RLS.General.Updateable
{
    public interface IUpdateable
    {
        public void AppendToUpdater();
        public void Update();
    }
}