using System.Collections.Generic;
using UnityEngine;

namespace RLS.General.Updateable
{
    public class Updater : MonoBehaviour
    {
        private List<IUpdateable> _updatables = new List<IUpdateable>();

        public void Append(IUpdateable updateable)
        {
            if (_updatables.Contains(updateable))
            {
                Debug.LogError($"Updater already contains {updateable}");
                return;
            }
            
            _updatables.Add(updateable);
        }

        private void Update()
        {
            for (int i = _updatables.Count - 1; i >= 0; i--)
            {
                if (_updatables[i] == null)
                {
                    _updatables.RemoveAt(i);
                    continue;
                }
                
                _updatables[i].Update();
            }
        }
    }
}