using RLS.Gameplay;

namespace RLS.InputSystem
{
    public class DragProcess
    {
        public readonly Actor DraggingActor;
        
        public DragProcess(Actor draggingActor)
        {
            DraggingActor = draggingActor;
        }
    }
}