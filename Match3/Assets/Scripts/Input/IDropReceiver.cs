using RLS.Gameplay;

namespace RLS.InputSystem
{
    public interface IDropReceiver
    {
        public bool TryReceive(Actor actor);
    }
}