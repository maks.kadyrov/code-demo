using RLS.Gameplay.ActorParameters;
using RLS.Gameplay.Behaviours;
using UnityEngine;
namespace RLS.InputSystem
{
    public class InputManager : MonoBehaviour
    {
        private Camera _camera;
        private DragProcess _dragProcess;

        private void Awake()
        {
            _camera = Camera.main;
        }

        private void Update()
        {
            TryDrag();
            TryDrop();
        }
        
        private void TryDrag()
        {
#if UNITY_EDITOR
            if (Input.GetMouseButtonDown(0))
#else
            if (Input.GetTouch(0).phase.Equals(TouchPhase.Began))
#endif
            {
#if UNITY_EDITOR
                var screenPosition = Input.mousePosition;
#else
                var screenPosition = Input.GetTouch(0).position;
#endif
                var worldPosition = _camera.ScreenToWorldPoint(screenPosition);

                if (Physics.Raycast(worldPosition, _camera.transform.forward, out var hit))
                {
                    var actorBehaviour = hit.collider.gameObject.GetComponent<ActorBehaviour>();

                    if (actorBehaviour.Actor is IDraggable)
                    {
                        _dragProcess = new DragProcess(actorBehaviour.Actor);
                    }
                }
            }
        }

        private void TryDrop()
        {
#if UNITY_EDITOR
            if (Input.GetMouseButton(0))
#else
            if (Input.GetTouch(0).phase.Equals(TouchPhase.Ended))
#endif
            {
                if (_dragProcess == null) return;
#if UNITY_EDITOR
                var screenPosition = Input.mousePosition;
#else
                var screenPosition = Input.GetTouch(0).position;
#endif
                var worldPosition = _camera.ScreenToWorldPoint(screenPosition);

                var hits = Physics.RaycastAll(worldPosition, _camera.transform.forward);

                foreach (var hit in hits)
                {
                    var receiver = hit.collider.gameObject.GetComponent<IDropReceiver>();
                    
                    if (receiver == null) continue;

                    if (receiver.TryReceive(_dragProcess.DraggingActor))
                    {
                        _dragProcess = null;
                        break;
                    }
                }
            }
        }
    }
}