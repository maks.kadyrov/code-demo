using RLS.Gameplay;
using RLS.General.Updateable;
using RLS.Signals;
using UnityEngine;
using Zenject;

[CreateAssetMenu(fileName = "BootstrapInstaller", menuName = "Installers/BootstrapInstaller")]
public class BootstrapInstaller : ScriptableObjectInstaller<BootstrapInstaller>
{
    [SerializeField] private Updater _updaterPrefab;
    [SerializeField] private CameraBehaviour _cameraBehaviourPrefab;
    
    public override void InstallBindings()
    {
        SignalBusInstaller.Install(Container);
        DeclareSignals();
        
        Container.Bind<Updater>().FromComponentInNewPrefab(_updaterPrefab).AsSingle();
        Container.Bind<CameraBehaviour>().FromComponentInNewPrefab(_cameraBehaviourPrefab).AsSingle().NonLazy();
    }

    private void DeclareSignals()
    {
        Container.DeclareSignal<ActionProcessedSignal>();
        Container.DeclareSignal<TileCreatedSignal>();
        Container.DeclareSignal<ActorSpawnSignal>();
        Container.DeclareSignal<ActorDestructedSignal>();
        Container.DeclareSignal<BoardUpdatedSignal>();
        Container.DeclareSignal<BoardSetupSignal>();
        Container.DeclareSignal<ActorMovedSignal>();
        Container.DeclareSignal<EffectProcessedSignal>();
    }
}