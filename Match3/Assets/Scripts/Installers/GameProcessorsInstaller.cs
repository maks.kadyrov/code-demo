using RLS.Gameplay;
using UnityEngine;
using Zenject;

[CreateAssetMenu(fileName = "GameProcessorsInstaller", menuName = "Installers/GameProcessorsInstaller")]
public class GameProcessorsInstaller : ScriptableObjectInstaller<GameProcessorsInstaller>
{
    public override void InstallBindings()
    {
        Container.Bind<MatchProcessor>().FromNew().AsSingle();
        Container.Bind<MatchBonusProcessor>().FromNew().AsSingle();
        Container.Bind<ActorsDropProcessor>().FromNew().AsSingle();
        Container.Bind<ActorsFillProcessor>().FromNew().AsSingle();
        Container.Bind<ActorActionProcessor>().FromNew().AsSingle();
        Container.Bind<EffectsProcessor>().FromNew().AsSingle();
    }
}