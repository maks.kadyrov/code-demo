using RLS.Gameplay;
using RLS.Gameplay.Behaviours;
using RLS.Gameplay.Factories;
using RLS.VisualProcesses;
using UnityEngine;
using UnityEngine.Serialization;
using Zenject;

[CreateAssetMenu(fileName = "GameplayInstaller", menuName = "Installers/GameplayInstaller")]
public class GameplayInstaller : ScriptableObjectInstaller<GameplayInstaller>
{
    [SerializeField] private VisualProcessor _visualProcessorPrefab;
    
    public override void InstallBindings()
    {
        Container.Bind<ActionProcessor>().FromNew().AsSingle().NonLazy();
        Container.Bind<VisualProcessor>().FromComponentInNewPrefab(_visualProcessorPrefab).AsSingle();
        
        Container.BindFactory<Actor, ActorBehaviour, ActorBehaviourFactory>().FromFactory<CustomActorBehaviourFactory>();
        Container.BindFactory<ActorType, Vector2Int, Actor, ActorFactory>().FromFactory<CustomActorFactory>();
        Container.BindFactory<Vector2Int, Tile, TileFactory>().FromFactory<CustomTileFactory>();

        Container.Bind<ActorSpawner>().FromNew().AsSingle();
        Container.Bind<Board>().FromNew().AsSingle();
    }
}