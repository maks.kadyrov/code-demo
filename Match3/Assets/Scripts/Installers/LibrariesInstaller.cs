using RLS.Libraries;
using UnityEngine;
using Zenject;

[CreateAssetMenu(fileName = "LibrariesInstaller", menuName = "Installers/LibrariesInstaller")]
public class LibrariesInstaller : ScriptableObjectInstaller<LibrariesInstaller>
{
    [SerializeField] private ActorsLibrary _actorsLibrary;
    
    public override void InstallBindings()
    {
        Container.Bind<ActorsLibrary>().FromInstance(_actorsLibrary).AsSingle();
    }
}