using System.Collections.Generic;
using RLS.Gameplay;
using UnityEngine;

namespace RLS.Editor
{
    [CreateAssetMenu(menuName = "Level Editor/Level Setup", fileName = "new_level_setup")]
    public class LevelSetup : ScriptableObject
    {
        public List<LevelSetupActor> SetupActors => new List<LevelSetupActor>(_setupActors);
        [SerializeField] private List<LevelSetupActor> _setupActors;
        [SerializeField] private bool _isForceRandom;

        private void OnValidate()
        {
            if (_isForceRandom)
            {
                _isForceRandom = false;
                _setupActors.Clear();
                
                for (int x = 0; x < 7; x++)
                {
                    for (int y = 0; y < 7; y++)
                    {
                        _setupActors.Add(new LevelSetupActor(ActorType.Random, new Vector2Int(x, y)));
                    }
                }
            }
        }
    }
}

