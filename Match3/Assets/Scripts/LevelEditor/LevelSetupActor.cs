using System;
using RLS.Gameplay;
using UnityEngine;

namespace RLS.Editor
{
    [Serializable]
    public class LevelSetupActor
    {
        [field:SerializeField] public Vector2Int Position { get; private set; }
        [field:SerializeField] public ActorType ActorType { get; private set; }

        public LevelSetupActor(ActorType type, Vector2Int position)
        {
            ActorType = type;
            Position = position;
        }
    }
}