using System.Linq;
using RLS.Gameplay;
using RotaryHeart.Lib.SerializableDictionary;
using UnityEngine;

namespace RLS.Libraries
{
    [CreateAssetMenu(menuName = "Library/Actors Library", fileName = "actors_library")]
    public class ActorsLibrary : ScriptableObject
    {
        [SerializeField] private SerializableDictionaryBase<ActorType, ActorsLibraryData> _actors;

        public bool TryGetData(ActorType type, out ActorsLibraryData data)
        {
            data = null;
            
            if (_actors.Any(item => item.Key.Equals(type)))
            {
                data = _actors[type];
                return true;
            }

            return false;
        }
    }
}