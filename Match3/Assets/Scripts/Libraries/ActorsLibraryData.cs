using System;
using RLS.Gameplay.Behaviours;
using UnityEngine;

namespace RLS.Libraries
{
    [Serializable]
    public class ActorsLibraryData
    {
        [field:SerializeField] public ActorBehaviour Prefab { get; private set; }
    }
}