using RLS.Gameplay;

namespace RLS.Signals
{
    public class ActionPreparedSignal : ActionSignal
    {
        public ActionPreparedSignal(GameAction action) : base(action){}
    }
}