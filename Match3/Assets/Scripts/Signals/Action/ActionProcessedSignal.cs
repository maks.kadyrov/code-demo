using RLS.Gameplay;

namespace RLS.Signals
{
    public class ActionProcessedSignal : ActionSignal
    {
        public ActionProcessedSignal(GameAction action) : base(action){}
    }
}