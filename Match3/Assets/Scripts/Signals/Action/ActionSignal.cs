using RLS.Gameplay;

namespace RLS.Signals
{
    public class ActionSignal : ISignal
    {
        public GameAction Action { get; }

        public bool Is<T>(out T signal) where T : GameAction
        {
            signal = Action as T;
            return Action is T;
        }
        
        public ActionSignal(GameAction action)
        {
            Action = action;
        }
    }
}