using RLS.Gameplay;

namespace RLS.Signals
{
    public class ActorDestructedSignal : ActorSignal
    {
        public ActorDestructedSignal(Actor actor) : base(actor){}
    }
}