using RLS.Gameplay;

namespace RLS.Signals
{
    public class ActorMovedSignal : ActorSignal
    {
        public readonly Tile Tile;

        public ActorMovedSignal(Actor actor, Tile toTile) : base(actor)
        {
            Tile = toTile;
        }
    }
}