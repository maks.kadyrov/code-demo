using RLS.Gameplay;

namespace RLS.Signals
{
    public class ActorSignal : ISignal
    {
        public readonly Actor Actor;
        
        public ActorSignal(Actor actor)
        {
            Actor = actor;
        }
    }
}