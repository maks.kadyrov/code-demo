using RLS.Gameplay;

namespace RLS.Signals
{
    public class ActorSpawnSignal : ActorSignal
    {
        public ActorSpawnSignal(Actor actor) : base(actor){}
    }
}