using RLS.Gameplay;

namespace RLS.Signals
{
    public class BoardSetupSignal : ISignal
    {
        public readonly Board Board;

        public BoardSetupSignal(Board board)
        {
            Board = board;
        }
    }
}