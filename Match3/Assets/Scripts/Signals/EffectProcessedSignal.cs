using RLS.Gameplay;

namespace RLS.Signals
{
    public class EffectProcessedSignal : ISignal
    {
        public readonly GameEffect Effect;

        public EffectProcessedSignal(GameEffect effect)
        {
            Effect = effect;
        }
    }
}