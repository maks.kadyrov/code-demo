using RLS.Gameplay;

namespace RLS.Signals
{
    public class TileCreatedSignal : ISignal
    {
        public readonly Tile Tile;
        
        public TileCreatedSignal(Tile tile)
        {
            Tile = tile;
        }
    }
}