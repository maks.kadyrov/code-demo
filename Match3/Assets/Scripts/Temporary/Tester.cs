using RLS.Gameplay;
using UnityEngine;
using Zenject;

namespace RLS.Temporary
{
    public class Tester : MonoBehaviour
    {
        [Inject] private SignalBus _signalBus;
        [Inject] private ActionProcessor _actionProcessor;
    }
}